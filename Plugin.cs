#if TOOLS
using Godot;
using System.Collections.Generic;

namespace Wings.Editor
{
    [Tool]
    public class Plugin : EditorPlugin
    {
        public const string PATH_PLUGIN_DIR = "res://addons/wing-components/";
        public const string PATH_COMPONENT_DIR = PATH_PLUGIN_DIR+"scripts/";

        public const string PATH_USER_COMPONENT_INFO_JSON       = "res://WingsComponentInfo.json";
        public const string PATH_INBUILT_COMPONENT_INFO_JSON    = PATH_PLUGIN_DIR + "WingsInbuiltComponentInfo.json";

        public const string WING_ICON_SMALL_FILE_NAME = "icon_small.png";

        public override void _EnterTree()
        {
            GetAllComponentInfo()?.ForEach(
                x =>
                {
                    var disp_name = x.Item1;
                    var script = x.Item2;
                    var icon = x.Item3;
                    AddToCreateNewNodeUI(disp_name, script, icon);
                }
            );
            GD.Print("Wings Editor: Initialised");
        }

        public override void _ExitTree()
        {
            GetAllComponentInfo()?.ForEach(
                x =>
                {
                    var disp_name = x.Item1;
                    RemoveCustomType(disp_name);
                });

            GD.Print("Wings Editor: De-Initialised");
        }

        // private
        List<(string, string, string)> GetAllComponentInfo()
        {
            var _result = new List<(string, string, string)>();
            
            var _user_components    = EditorUtils.GetComponentInfoFromJsonFile(PATH_USER_COMPONENT_INFO_JSON);
            var _inbuilt_components = EditorUtils.GetComponentInfoFromJsonFile(PATH_INBUILT_COMPONENT_INFO_JSON);
            
            _result.AddRange(_inbuilt_components);
            if(_user_components != null)
                _result.AddRange(_user_components);

            return _result;
        }

        void AddToCreateNewNodeUI(string display_name, string script, string icon)
        {
            var _icon = string.IsNullOrEmpty(icon) ? 
            PATH_PLUGIN_DIR+WING_ICON_SMALL_FILE_NAME : PATH_PLUGIN_DIR+icon;
            
            EditorUtils.AddCustomType(this, display_name, script, _icon );
        }
        
    }
}

#endif