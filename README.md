# wings-components
Wings is a collection of useful general purpose components which promotes editor centric workflow
i.e. data driven way of setting up node behaviours. It also provides apis to let developers extend and
write custom components for their custom needs.

Aim:
Reusability, maintainability and simplicity
- Small general purpose components that are hooked up in editor to create expected (complex) behaviours
- Handle increase in feature complexity (overtime) without having to write code (or atleast less code)
- Improve collaboration. Artists and designers (with little training) can contribute to setting up the
    expected behaviours without having to rely on the programmer.

# Sample Project
https://gitlab.com/wings-code/wing-components-sample

# How to install
- Copy or clone repo contents into res://addons/wing-components
- Activate plugin in Project Settings -> Plugins -> Wings Godot Components