using Godot;
using System.Collections.Generic;
using Wings.Utils;
using Array = Godot.Collections.Array;

namespace Wings.Editor
{
    public class EditorUtils
    {
        public static EditorPlugin AddCustomType(EditorPlugin instance, string type, string path_script, string icon_path = null)
        {
            // Initialization of the plugin goes here
            // Add the new type with a name, a parent type, a script and an icon
            var script = GD.Load<Script>(path_script);
            var texture = GD.Load<Texture>(icon_path);
            instance.AddCustomType(type, "Node" , script, texture);
            
            return instance;
        }

        public static List<(string, string, string)> GetComponentInfoFromJsonFile(string file_path)
        {
            string json_str = CommonUtils.GetTextFromFile(file_path);
            if(json_str == null)
            {
                GD.PrintErr("Wings Editor: No component info found at, "+file_path);
                return null;
            }
            
            return GetComponentInfoFromJsonString(json_str);
        }

        public static List<(string, string, string)> GetComponentInfoFromJsonString(string json)
        {

            var data = JSON.Parse(json);
            var parsed = data.Result as Array;

            var result = new List<(string, string, string)>();
            foreach (var i in parsed)
            {
                Array comp_info = i as Array;

                string display_name = comp_info[0] as string;
                string script_path = comp_info[1] as string;
                string icon_path = comp_info.Count > 2 
                ? comp_info[2] as string : string.Empty;

                result.Add((display_name, script_path, icon_path));
            }

            return result;

        }
    }
}