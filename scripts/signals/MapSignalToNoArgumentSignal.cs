/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;

namespace Wings
{
    /// <summary>
    /// Converts signal calls with arguments into a no argument signal.
    /// </summary>
    public class MapSignalToNoArgumentSignal : Node
    {
        [Signal]
        public delegate void OnEvent();

        public void Fire(params object[] arg)
        {
            EmitSignal(nameof(OnEvent));
        }
    }
}