/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class NumberConditionSignal : BehaviourWithoutTick<NumberData>
    {
        [Signal] public delegate void OnTrue();
        [Signal] public delegate void OnFalse();

        [Export] public Conditions Condition;
        [Export] public float Value;

        public override void _Ready()
        {
            base._Ready();

            Target.OnChange += (v) =>
            {
                bool _true = false;
                if(Condition == Conditions.EQUAL_TO)
                    _true = v == Value;
                else if(Condition == Conditions.GREATER_THAN)
                    _true = v > Value;
                else if(Condition == Conditions.LESSER_THAN)
                    _true = v < Value;

                Print("On Value Change, "+v+" Condition, "+Condition+" "+Value+", "+_true);
                string signal = _true ? nameof(OnTrue) : nameof(OnFalse);
                EmitSignal(signal);
            };
        }

        public enum Conditions
        {
            EQUAL_TO,
            GREATER_THAN,
            LESSER_THAN
        }
    }
}