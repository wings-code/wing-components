/*********************************************************
 * @created on Fri May 29 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class InputSignal : Behaviour<Node>
    {
        // editor
        [Export] string Action="";
        [Export] float HoldRepeatIntervalSec = 0f;

        // signals
        [Signal] delegate void OnPressed();
        [Signal] delegate void OnHold();
        [Signal] delegate void OnRelease();

        public override void _PhysicsProcess(float delta)
        {
            if (_prev_state != Input.IsActionPressed(Action))
            {
                _prev_state = Input.IsActionPressed(Action);

                if(_prev_state)
                    EmitSignal(nameof(OnPressed));
                else
                    EmitSignal(nameof(OnRelease));
            }

            if(_prev_state 
            && (OS.GetTicksMsec() - _prev_hold_repeat  > HoldRepeatIntervalSec * 1000))
            {
                EmitSignal(nameof(OnHold));
                _prev_hold_repeat = OS.GetTicksMsec();
            }
        }

        // private
        bool _prev_state = false;
        float _prev_hold_repeat = 0;
    }
}