/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;

namespace Wings
{
    /// <summary>
    /// Experimental*
    /// </summary>
    public class ExpressionSignal : Behaviour<Node2D>, IExecutable
    {
        [Signal] public delegate void On(bool status);
        [Signal] public delegate void OnTrue();
        [Signal] public delegate void OnFalse();

        // editor
        [Export(PropertyHint.MultilineText)] public string Expression = ""; 

        public void Execute()
        {
            // Expression
            // stub
        }
    }
}