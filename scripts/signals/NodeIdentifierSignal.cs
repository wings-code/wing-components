/*********************************************************
 * @created on Sun Jun 07 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;

namespace Wings
{
    public class NodeIdentifierSignal : DataBusSignal
    {
        [Signal]
        public delegate void OnNodeChange(Node value);
        [Signal]
        public delegate void OnNodePathChange(NodePath value);

        public override void OnChange(object value, object sender)
        {
            var node = value as Node;
            EmitSignal(nameof(OnNodeChange), node);
            EmitSignal(nameof(OnNodePathChange), node.GetPath());
        }      
    }
}