/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class NumberData : Data<float>
    {
        [Signal] 
        public delegate void OnNumberChange(float value);

        // public
        public void Add(int value)
        {
            Value = Value + value;
        }

        public void Multiply(float value)
        {
            Value = Value * value;
        }

        // protected
        protected override void OnEmitSignal()
        {
            EmitSignal(nameof(OnNumberChange), Value);
        }
    }
}