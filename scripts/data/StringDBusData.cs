/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class StringDBusData : StringData
    {
        [Export] public string Default = "";
        [Export] public string DataBusId = "";
        [Export] public bool EmitSignalOnEnable = true;

        public bool CanUseCustomDataBus => !string.IsNullOrEmpty(DataBusId);

        public override string Value
        {
            get => GetDBusValue(DataBusId, Default);
            set
            {

                if (!Value.Equals(value))
                {
                    SetDBusValue(DataBusId, value);
                }
            }
        }

        public override void _Enabled()
        {
            if(EmitSignalOnEnable)
            {
                OnChange?.Invoke(Value);
                OnEmitSignal();
            }

            DataBus.OnChange += OnDBusValueChange;
        }

        public override void _Disabled()
        {
            DataBus.OnChange -= OnDBusValueChange;
        }

        // private        
        void OnDBusValueChange(string id, object value, object sender)
        {
            if(id != DataBusId)
                return;

            OnChange?.Invoke(Value);
            OnEmitSignal();
       }

        string GetDBusValue(string db_key, string default_value = "")
        {
            object v = DataBus.Get(db_key);
            return v != null ? v.ToString() : default_value;
        }

        void SetDBusValue(string db_key, object value)
        {
            DataBus.Set(db_key, value, this);
        }
    }
}