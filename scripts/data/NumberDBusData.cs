/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class NumberDBusData : NumberData
    {
        [Export] public float Default = 0;
        [Export] public string DataBusId = "";
        [Export] public bool EmitSignalOnEnable = true;

        public bool CanUseCustomDataBus => !string.IsNullOrEmpty(DataBusId);

        public override float Value
        {
            get => GetDBusValue(DataBusId, Default);
            set
            {

                if (!Value.Equals(value))
                {
                    SetDBusValue(DataBusId, value);
                }
            }
        }

        public override void _Enabled()
        {
            if(EmitSignalOnEnable)
            {
                OnChange?.Invoke(Value);
                OnEmitSignal();
            }

            DataBus.OnChange += OnDBusValueChange;
        }

        public override void _Disabled()
        {
            DataBus.OnChange -= OnDBusValueChange;
        }

        // private        
        void OnDBusValueChange(string id, object value, object sender)
        {
            if(id != DataBusId)
                return;

            OnChange?.Invoke((float) value);
            OnEmitSignal();
       }

        float GetDBusValue(string db_key, float default_value = 0)
        {
            object v = DataBus.Get(db_key);
            return v != null ? (float)v : default_value;
        }

        void SetDBusValue(string db_key, object value)
        {
            DataBus.Set(db_key, value, this);
        }
    }
}