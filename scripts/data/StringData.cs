/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class StringData : Data<string>
    {
        [Signal] 
        public delegate void OnStringChange(string value);

        protected override void OnEmitSignal()
        {
            EmitSignal(nameof(OnStringChange), Value);
        }
    }
}