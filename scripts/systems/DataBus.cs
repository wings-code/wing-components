/*********************************************************
 * @created on Sat Jun 06 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using System;
using System.Collections.Generic;
using Godot;
using Wings.Utils;

namespace Wings
{
    using Array = Godot.Collections.Array;
    public class DataBus
    {
        public const string PATH_DEFAULT_VALUE_JSON = "res://WingsDataBusDefaults.json";

        public static Action<string, object, object> OnChange;

        public static object Get(string id)
        {
            if (_init == false)
            {   
                // experimental*
                PopulateDefaultValue(PATH_DEFAULT_VALUE_JSON);
                _init = true;
            }

            if (m_Data.ContainsKey(id))
                return m_Data[id];
            else if(m_Default.ContainsKey(id))
                return m_Default[id];

            return null;
        }

        public static void Set(string id, object value, object sender)
        {
            if (string.IsNullOrEmpty(id))
            {
                GD.PrintErr("DataBus: Cannot set with empty/null id, sender " + sender);
                return;
            }

            if (!m_Data.ContainsKey(id))
                m_Data.Add(id, null);

            m_Data[id] = value;
            GD.Print("DataBus: Set value for, " + id + ", value " + value + ", sender " + sender);
            OnChange?.Invoke(id, value, sender);
        }

        // privates
        static bool _init = false;
        static Dictionary<string, object> m_Data = new Dictionary<string, object>();
        static Dictionary<string, object> m_Default = new Dictionary<string, object>();

        static void PopulateDefaultValue(string path)
        {
            JSONParseResult json = CommonUtils.GetJSONFromFile(path);
            if(json == null)
                return;

            m_Default = GetDefaultValueDictFromJson(json);
        }

        static Dictionary<string, object> GetDefaultValueDictFromJson(JSONParseResult json_object)
        {

            var parsed = json_object.Result as Array;

            var result = new Dictionary<string, object>();
            foreach (var i in parsed)
            {
                Array kv = i as Array;

                string key = kv[0] as string;
                string type = kv[1] as string;
                object value = null;
                switch(type)
                {
                    case "number":
                    value = float.Parse(kv[2] as string);
                    break;
                    case "string":
                    value = kv[2] as string;
                    break;
                }

                result.Add(key, value);
            }

            return result;
        }
    }
}