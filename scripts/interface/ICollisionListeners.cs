/*********************************************************
 * @created on Fri May 29 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using System.Collections.Generic;
using Godot;

namespace Wings
{
     // kinematic 2d
    public interface IKinematicCollision2DEntered :IListener
    {
        void OnCollisionEntered(List<KinematicCollision2D> col);
    }
     public interface IKinematicCollision2DExited : IListener
    {
        void OnCollisionExited();
    }
}