/*********************************************************
 * @created on Sat Jun 06 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
namespace Wings
{
    public interface IExecutable : IListener
    {
         void Execute();
    }
}