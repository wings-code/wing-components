/*********************************************************
 * @created on Sun June 07 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    // setter listener
    public interface ISetInt :IListener
    {
        void Set(int value);
    }
    public interface ISetFloat :IListener
    {
        void Set(float value);
    }
    public interface ISetString :IListener
    {
        void Set(string value);
    }
    public interface ISetVector2 :IListener
    {
        void Set(Vector2 value);
    }
}