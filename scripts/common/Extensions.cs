using System;
using Godot;
using Wings.Component;
using Wings.Utils;

namespace Wings
{
    public static class Extensions
    {
        public static void Invoke<T>(this WingEvent w_event, Node invoker, Action<T> call_map) where T : class, IListener
        {
            CommonUtils.Invoke<T>(invoker, w_event, call_map);
        }


        public static U FindChildOfType<U>(this IComponent comp) where U : Node
        {
            return CommonUtils.FindChildOfType<U>(comp as Node);
        }

        public static AnimationNodeStateMachinePlayback GetPlayback(this AnimationTree tree)
        {
            return tree.Get("parameters/playback") as AnimationNodeStateMachinePlayback;
        }
    }
}