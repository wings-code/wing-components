using System;
using System.Collections.Generic;
using System.Linq;
using Godot;
using Godot.Collections;

namespace Wings.Utils
{
    public class CommonUtils
    {
         public static U FindChildOfType<U>(Node root) where U : Node
        {
            var array = root.GetChildren();
            foreach(var a in array)
            {
                if (a is U)
                    return a as U;
            }
            
            return null;
        }

        public static List<KinematicCollision2D> GetSlideCollisions(int count, KinematicBody2D kbody)
        {
            var _col = new List<KinematicCollision2D>();
            foreach( var i in Enumerable.Range(0, count))
            {
                GD.Print(i);
                _col.Add(kbody.GetSlideCollision(i));
            }

            return _col;
        }

        // event util
        public static void Invoke<T>(Node invoker, WingEvent w_event, Action<T> call_map) where T : class, IListener
        {
            foreach(var i in w_event)
            {
                Node node = invoker.GetNode(i);
                T listener =  node as T;
                if (listener != null)
                    call_map(listener);
                else
                    GD.PrintErr($"Error: Cannot Invoke {node.Name}, does not derive from "+typeof(T));
            }
        }

        // file
        public static string GetTextFromFile(string file_path)
        {
             File _file = new File();
            if(!_file.FileExists(file_path))
                return null;

            _file.Open(file_path, File.ModeFlags.Read);

            string _text = _file.GetAsText();
            _file.Close();

            return _text;
        }
        public static JSONParseResult GetJSONFromFile(string file_path)
        {
            string _json_str = GetTextFromFile(file_path);
            return !string.IsNullOrEmpty(_json_str) ? JSON.Parse(_json_str) : null;
        }
    }
}