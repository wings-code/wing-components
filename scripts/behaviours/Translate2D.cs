/*********************************************************
 * @created on Sat May 30 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class Translate2D : Behaviour<Node2D>
    {
        // editor
        [Export] float Modifier = 1f; // multiplier

        public void Set(float x, float y)
        {
            Set(new Vector2(x,y));
        }

        public void Set(Vector2 translation)
        {
            _translation = _translation + translation;
        }

        public override void _Process(float delta)
        {
            Target.Position = Target.Position + (_translation * Modifier * delta);
            _translation = Vector2.Zero;
        }

        // private
        Vector2 _translation = Vector2.Zero;

    }
}