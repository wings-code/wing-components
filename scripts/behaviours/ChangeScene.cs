/*********************************************************
 * @created on Sun Jun 07 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 

using Godot;
using Wings.Component;

namespace Wings
{
    public class ChangeScene : ComponentBase, IExecutable
    {
        // editor
        [Export] public string ScenePath = "";

        public void Set(string scene_path)
        {
            GetTree().ChangeScene(scene_path);
        }

        public void Execute()
        {
            Set(ScenePath);
        }
    }
}