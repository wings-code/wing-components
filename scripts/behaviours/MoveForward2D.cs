/*********************************************************
 * @created on Fri May 29 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;

namespace Wings
{
    public class MoveForward2D : Behaviour<Node2D>
    {
        [Export] public float Rate = 5f;

        // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _PhysicsProcess(float delta)
        {
            Target.Position = Target.Position + (Target.Transform.x * Rate * delta);
        }
    }
}