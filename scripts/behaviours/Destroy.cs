/*********************************************************
 * @created on Fri May 29 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class Destroy : BehaviourWithoutTick<Node>, IExecutable
    {
        public void Execute()
        {
            Target.QueueFree();
        }
    }
}