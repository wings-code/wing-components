/*********************************************************
 * @created on Fri May 29 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;

namespace Wings
{
    public class LookAtNode2D : Behaviour<Node2D>
    {
        [Export] NodePath Node2D;
        [Export] float Rate = 1f;

        public void Set(NodePath nodePath)
        {
            Node2D = nodePath;
            _node = GetNode(Node2D) as Node2D;
        }

        public override void _Ready()
        {
            base._Ready();
            Set(Node2D);
        }

        // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _Process(float delta)
        {
            if(_node == null || _node.IsQueuedForDeletion())
                return;

            var dest_pos = Target.Transform.XformInv(_node.GlobalPosition);
            Target.Rotation = Target.Rotation + dest_pos.Angle() * Rate;
        }

        // private
        Node2D _node;
    }
}