/*********************************************************
 * @created on Sun June 07 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class AnimationTreeTavel : BehaviourWithoutTick<AnimationTree>, IExecutable, ISetString
    {
        [Export] string StateName = "";
        
        public void Set(string value)
        {
            StateName = value;
        }

        public void Execute()
        {
            if(string.IsNullOrEmpty(StateName))
                return;

           var playback = Target.GetPlayback();
           playback.Travel(StateName);
        }

    }
}