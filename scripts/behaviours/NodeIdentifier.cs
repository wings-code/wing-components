/*********************************************************
 * @created on Sun Jun 07 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;

namespace Wings
{
    public class NodeIdentifier : BehaviourWithoutTick<Node>
    {
        [Export] string DataBusId = "";

        public override void _Enabled()
        {
            if (!string.IsNullOrEmpty(DataBusId))
                DataBus.Set(DataBusId, Target, this);
        }        
    }
}