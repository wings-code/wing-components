/*********************************************************
 * @created on Fri May 29 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;

namespace Wings
{
    public class MoveForward2DKinematicBody : Behaviour<KinematicBody2D>
    {
        [Export] public float Rate = 5f;
        
        // Useful to determing floor normal usually (0,-1) for 
        // 2D Platformers and (0,0) for top down since they 
        // only have walls in 2D
        [Export] public Vector2 UpDirection = new Vector2(0,-1); 

        // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _PhysicsProcess(float delta)
        {
            Target.MoveAndSlide(Target.Transform.x * Rate, upDirection: UpDirection);
        }
    }
}