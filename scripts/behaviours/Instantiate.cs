/*********************************************************
 * @created on Fri May 29 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings
{
    public class Instantiate : BehaviourWithoutTick<Node>, IExecutable
    {
        [Export] PackedScene Prefab;
        [Export] bool  ParentToTarget = true;

        public void Execute()
        {
            Node parent = ParentToTarget ? Target : GetTree().CurrentScene;
            Node instance = Prefab.Instance();

            //node 2d
            Node2D _target = Target as Node2D;
            Node2D _instance = instance as Node2D;

            if(_target != null
             && instance != null)
             {
                _instance.GlobalPosition = _target.GlobalPosition;
                _instance.GlobalRotation = _target.GlobalRotation;
             }
            
            parent?.AddChild(instance);
        }
    }
}