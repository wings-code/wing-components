/*********************************************************
 * @created on Fri May 29 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;

namespace Wings
{
    public class LookAtMouse2D : Behaviour<Node2D>
    {
        [Export] float Rate = 1f;

        // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _Process(float delta)
        {
            var mouse_pos = Target.GetLocalMousePosition();
            Target.Rotation = Target.Rotation + mouse_pos.Angle() * Rate;
        }
    }
}