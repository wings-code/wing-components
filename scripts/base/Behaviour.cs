/*********************************************************
 * @created on Sun May 24 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;
using Wings.Component;

namespace Wings
{
    /// <summary>
    /// Base class for components with tick. _Process and _PhysicsProcess calls can be 
    /// enabled/disabled using Enable property
    /// Note* base._Ready() should be called first, in derived class if _Ready() is used.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Behaviour<T> : BehaviourBase<T> where T : Node
    {
        // editor
        [Export] public bool Enabled {
            get => mEnabled;
            set 
            {
                if(mEnabled != value)
                {
                    Run(value);
                    mEnabled = value;
                }
            }}
        
        public override void _Ready()
        {
            base._Ready();
            Run(mEnabled);
        }

        public override void _Notification(int what)
        {
            if(NotificationPredelete == what)
                Enabled = false;
        }

        // privates
        bool mEnabled = true;

        void Run(bool state)
        {
            SetProcess(state);
            SetPhysicsProcess(state);
            
            if(state)
                _Enabled();
            else
                _Disabled();
        }
    }
}