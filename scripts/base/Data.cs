/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using System;
using System.Runtime.Serialization;
using Godot;
using Wings.Component;

namespace Wings
{
    /// <summary>
    /// Base class for data Components
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Data<T> : ComponentBase where T : IEquatable<T>
    {
        public Action<T> OnChange;

        [Export]
        public virtual T Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                if (!m_Value.Equals(value))
                {    
                    m_Value = value;

                    OnChange?.Invoke(value);
                    OnEmitSignal();
                }
            }
        }

        // protected
        protected abstract void OnEmitSignal();

        public override void _Notification(int what)
        {
            if(NotificationReady == what)
            {
                SetProcess(false);
                SetPhysicsProcess(false);

                _Enabled();
            }

            if(NotificationPredelete == what)
            {   
                _Disabled();
            }
        }

        T m_Value = default(T);
    }
}