/*********************************************************
 * @created on Sun May 24 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;
using Wings.Component;

namespace Wings
{
    /// <summary>
    /// experimental*
    /// Base class for all signal mappers.
    /// Maps the incoming call to IListener
    /// </summary>
    /// <typeparam name="T">Expected Listener Type</typeparam>
    public class SignalMap<T> : ComponentBase where T : class, IListener
    {
        [Export] private NodePath ListenerNodePath = "..";
        public T Listener{ get; private set; }

        public override void _Ready()
        {
            SetProcess(false);
            SetPhysicsProcess(false);
            Listener = GetNode(ListenerNodePath) as T;
        }   
    }
}