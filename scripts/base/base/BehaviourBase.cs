/*********************************************************
 * @created on Sun May 24 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;
using Wings.Utils;

namespace Wings.Component
{
    /// <summary>
    /// Base class for behaviour component that requires a target.
    /// Note* base._Ready() should be called first, in derived class if _Ready() is used.
    /// </summary>
    /// <typeparam name="T">Target node required</typeparam>
    public class BehaviourBase<T> : ComponentBase where T : Node
    {
        [Export] private NodePath TargetNodePath = "..";
        public T Target { get; private set; }

        public override void _Ready()
        {
            base._Ready();

            // TODO Change to editor check
            Target = GetNode(TargetNodePath) as T;
            if (Target == null)
                PrintError($"Target node not of type: {typeof(T).FullName}");
        }
    }
}