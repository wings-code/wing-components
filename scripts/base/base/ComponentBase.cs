/*********************************************************
 * @created on Sun Jun 07 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;

namespace Wings.Component
{
    /// <summary>
    /// Base class for all wings component.
    /// Note* base._Ready() should be called first, in derived class if _Ready() is used.
    /// </summary>
    public class ComponentBase : Node, IComponent
    {
        public override void _Ready()
        {
            DefaultPrefix = GetType().Name;
        }

        public virtual void _Enabled() { }
        public virtual void _Disabled() { }

        // protected
        protected string DefaultPrefix;
        protected void Print(string message, string prefix = null)
        {
            prefix = prefix == null ? DefaultPrefix : prefix;
            GD.Print(prefix + "-" + Name + ": " + message);
        }

        protected void PrintError(string message, string prefix = null)
        {
            prefix = prefix == null ? DefaultPrefix : prefix;
            GD.PrintErr(prefix + "-" + Name + ": " + message);
        }
    }
}