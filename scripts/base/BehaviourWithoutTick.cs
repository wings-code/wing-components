/*********************************************************
 * @created on Sun May 24 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/
 
using Godot;
using Wings.Component;

namespace Wings
{
    /// <summary>
    /// Base class for components without tick. Process and Physics Process wont be called
    /// Note* base._Ready() should be called first, in derived class if _Ready() is used.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BehaviourWithoutTick<T> : BehaviourBase<T> where T : Node
    {   
        public override void _Notification(int what)
        {
            if(NotificationReady == what)
            {
                SetProcess(false);
                SetPhysicsProcess(false);

                _Enabled();
            }

            if(NotificationPredelete == what)
                _Disabled();
        }

    }
}