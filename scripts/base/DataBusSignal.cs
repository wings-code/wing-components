/*********************************************************
 * @created on Sun May 31 2020 by pradeep kumar rajamanickam
 * @email   raj.pradeep.kumar@gmail.com
 *********************************************************/

using Godot;
using Wings.Component;

namespace Wings
{
    /// <summary>
    /// Base class for data bus signal components.
    /// </summary>
    public class DataBusSignal : ComponentBase
    {
        // editor
        [Export] public string DataBusId = "";
        [Export] public bool FireOnReady = true;
        
        public object Value => DataBus.Get(DataBusId);

        public override void _Ready()
        {
            if(FireOnReady 
            && !string.IsNullOrEmpty(DataBusId))
            {
                var value = Value;
                if(value != null)
                    OnChange(DataBusId, value, null);
            }

            DataBus.OnChange += OnChange;
        }

        public override void _ExitTree()
        {
            DataBus.OnChange -= OnChange;
        }

        // protected
        public virtual void OnChange(object value, object sender){}

        // private
        void OnChange(string id, object value, object sender)
        {
            if(id == DataBusId)
                OnChange(value, sender);
        }
    }
}